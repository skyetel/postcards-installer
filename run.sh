#!/bin/bash

# Exit on any error
set -e

##
### FUNCTIONS
##

error_log() {
    echo "$*" 1>&2
}

die() {
    error_log $* ; exit 1
}

handle_command_args() {
    local POSITIONAL=()
    while [[ $# -gt 0 ]]
    do
        local key="$1"

        case $key in
            -b|--backend-tag)
                # Allow a backend tag to be specified
                shift
                BACKEND_TAG=$1
                ;;
            -u|--ui-tag)
                # Allow a UI tag to be specified
                shift
                UI_TAG=$1
                ;;
            --rebuild-app)
                # Rebuild the application fully
                REBUILD_APP="true"
                ;;
            --rebuild-deps)
                # Rebuild only the application project parts
                REBUILD_DEPS="true"
                ;;
            -d|--run-debug)
                RUN_DEBUG="true"
                ;;
            *)    # unknown option
                POSITIONAL+=("$1") # save it in an array for later
                ;;
        esac
        shift
    done
    set -- "${POSITIONAL[@]}" # restore positional parameters

    return 0
}

# Add /usr/local/bin to the PATH
export PATH=$PATH:/usr/local/bin

# Get path to current script
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"

# Require root access
if [[ $EUID -ne 0 ]]; then
    die "This app must be run as root" 
fi

# Handle command-line arguments
handle_command_args "$@" || exit 1

# Perform logic steps as a result of command-line args
if [ "$RUN_DEBUG" = "true" ]; then
    echo "Running debug mode"
    OVERRIDE="docker-compose.debug.yml"
else
    OVERRIDE="docker-compose.override.yml"
fi

# Require dependent commands
hash wget 2>/dev/null || die "The command 'wget' is required for this aplication"
hash sh 2>/dev/null || die "The shell 'sh' is required for this aplication"
hash netstat 2>/dev/null || die "The command 'netstat' is required for this aplication"
hash grep 2>/dev/null || die "The command 'grep' is required for this aplication"
hash uname 2>/dev/null || die "The command 'uname' is required for this aplication"

# Run Get-Docker script if docker does not exist
hash docker 2>/dev/null || wget -nv -O- https://get.docker.com/ | sh \
    || (echo "Your operating system is not supported" && exit 1)

# Start docker if it is not already started
hash systemctl 2>/dev/null && systemctl start docker || (hash service 2>/dev/null && service docker start)

# Download and install docker-compose 1.23.2 if it does not exist
hash docker-compose 2>/dev/null \
    || (wget -O /usr/local/bin/docker-compose https://github.com/docker/compose/releases/download/1.23.2/docker-compose-`uname -s`-`uname -m` \
    && chmod +x /usr/local/bin/docker-compose)

# Check if configuration file is needed
if [ ! -f $DIR/config.env ]; then
    CONFIGURE="Y"

  # Set a random inbound SMS/MMS authorization either from Sha1sum or just a random number
  # Attempt these random string generators:
  # 1. Try "openssl rand" if possible
  # 2. Use $RANDOM with sha1sum
  # 3. Just use $RANDOM
  hash openssl 2>/dev/null && API_INBOUND_AUTH_KEY=${API_INBOUND_AUTH_KEY:-$(openssl rand -hex 12)} \
      || (hash awk 2>/dev/null && hash sha1sum 2>/dev/null \
      && API_INBOUND_AUTH_KEY=${API_INBOUND_AUTH_KEY:-$(echo $RANDOM | sha1sum | awk '{print $1}')}) \
      || API_INBOUND_AUTH_KEY=${API_INBOUND_AUTH_KEY:-$RANDOM}

  CONFIGURATION="API_INBOUND_AUTH_KEY=${API_INBOUND_AUTH_KEY}\n"

else
    read -n 1 -r -p "Would you like to reconfigure (Y/N)? " RESPONSE
    echo
    echo
    if [[ $RESPONSE =~ ^[Yy]$ ]]; then
        CONFIGURE="Y"
        source $DIR/config.env

        CONFIGURATION="API_INBOUND_AUTH_KEY=$(grep -oP '(?<=API_INBOUND_AUTH_KEY=).*' $DIR/config.env)\n"
    fi
fi



if [ "$CONFIGURE" = "Y" ]; then
    # Perform configuration
    source $DIR/config-check.env
    for KEY in "${ORDER[@]}"; do
        while [ true ]; do
            eval DEFVAL="\$$KEY"
            if [ "$DEFVAL" != "" ]; then
                PROMPT="${CONFIG[$KEY]} [$DEFVAL] "
            else
                PROMPT="${CONFIG[$KEY]} " 
            fi
            read -e -p "$PROMPT" RESPONSE
            RESPONSE="${RESPONSE:-$DEFVAL}"

            if [ "${RE[$KEY]}" != "" ] && [[ $RESPONSE =~ ${RE[$KEY]} ]]; then
                CONFIGURATION="${CONFIGURATION}${KEY}=$RESPONSE\n"
                eval $KEY="$RESPONSE"
                break
            else
                echo "Invalid. Try again."
            fi
        done
    done
    
    # Handle static values
    for KEY in "${!STATIC[@]}"; do
        eval VAL="${STATIC[$KEY]}"
        CONFIGURATION="${CONFIGURATION}${KEY}=${VAL}\n"
    done

    # Handle reference values (same as another answer)
    for KEY in "${!REF[@]}"; do
        eval VAL="\$${REF[$KEY]}"
        CONFIGURATION="${CONFIGURATION}${KEY}=${VAL}\n"
    done

    echo -e "${CONFIGURATION}" > $DIR/config.env
fi

source $DIR/config.env 2>/dev/null || die "Something is wrong with your configuration. Please backup $DIR/config.env, delete it, and run this app"

# Check if it is running already by comparing service with running services
RUNNING_SERVICES="$(docker-compose -f $DIR/docker-compose.yml -f $DIR/$OVERRIDE ps --services --filter status=running | sort)" 
LIST_SERVICES="$(docker-compose -f $DIR/docker-compose.yml -f $DIR/$OVERRIDE ps --services | sort)" 
if [ "$RUNNING_SERVICES" = "$LIST_SERVICES" ]; then
    echo "The app is running now"
    read -n 1 -r -p "Would you like to restart it (Y/N)? " RESPONSE
    echo
    echo
    if [[ $RESPONSE =~ ^[Yy]$ ]]; then
        # Take it down
        docker-compose -f $DIR/docker-compose.yml -f $DIR/$OVERRIDE down &> /dev/null && echo "App stopped"
    fi
elif [ "$RUNNING_SERVICES" != "" ]; then
    # TODO: Some services are running but not all of them so it might be best to force a restart?
    # Do nothing but continue to perform "up" which should solve this
    echo "WARNING: Service is partially running"
else
    # Check if required ports are available
    netstat -tln 2> /dev/null | grep -q ":443 " && die "Another application is already using port 443 (HTTPS)"
    netstat -tln 2> /dev/null | grep -q ":80 "  && die "Another application is already using port 80 (HTTP)"
fi

# TODO: Handling upgrades
# Check if currently on git tag: git describe --exact-match --tags $(git log -n1 --pretty='%h')

# Force rebuild if needed
if [ "$REBUILD_APP" = "true" ]; then
    echo "Rebuilding app..."
    ADDL_OVERRIDE="--no-cache"
elif [ "$REBUILD_DEPS" = "true" ]; then
    echo "Rebuilding dependencies..."
    ADDL_OVERRIDE="--build-arg REBUILD_DEPS=$(date +%s)"
fi

# Attempt to quietly build the application
echo "This may take a few minutes the first time. Please be patient..."
docker-compose -f $DIR/docker-compose.yml -f $DIR/$OVERRIDE build \
    --build-arg UI_TAG=${UI_TAG:-master} \
    --build-arg BACKEND_TAG=${BACKEND_TAG:-master} \
    --build-arg REACT_APP_TITLE=${REACT_APP_TITLE:-Postcards} \
    --build-arg REACT_APP_LOGO_URL=${REACT_APP_LOGO_URL:-https://postcards.skyetel.com/favicon.png} \
    ${ADDL_OVERRIDE} \
    app 2>&1 > /dev/null || die "Building the app failed :("

# Finally, start the application
docker-compose -f $DIR/docker-compose.yml -f $DIR/$OVERRIDE up -d &> /dev/null && echo "App started"

echo "In a few moments visit https://$LETSENCRYPT_HOST/backend/admin in your browser"
echo
echo "Username: admin"
echo "Password: $ADMIN_BACKEND_PASSWORD"
echo
echo "----------------"
echo
echo "Setup your Skyetel SMS & MMS setting on your selected phonenumbers as follows: "
echo
echo "Forward:  Callback URL"
echo "URL:      https://$LETSENCRYPT_HOST/backend/api/in?key=$API_INBOUND_AUTH_KEY"
echo "Method:   POST"
