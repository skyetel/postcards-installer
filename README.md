# Postcards
This application was created just for you to take a chance to send someone a thoughtful SMS or a MMS picture to remind them just how awesome they are.

Reading this will prepare you to install and start your brand new Postcards application and get on the road to brightening someone's day.

![Example of Postcards User Interface](docs/postcards-example1.png)

Visit our official documentation by [jumping over to the Postcards documentation site](https://skyetel.atlassian.net/wiki/spaces/SUG/pages/649134085/Postcards).

## Things you will need
* A Skyetel phone and SMS/MMS account. If you don't have one, visit www.skyetel.com and try it.
    * A Skyetel phonenumber with SMS/MMS feature enabled
    * Inside your Skyetel account retrieve your API SID and Secret to send messages
* Super user access to a Linux-based server that does not already host a website
    * We reccomend CentOS 7 or Debian 8, but should also work on most Debian-based servers. Centos 8 will not work.
    * Windows servers are not currently supported
    * Due to the number of services (4-5) that support Postcards, we recommend using at least 4 GB of memory
* A public domain name like myserver.example.com that points to your server


## Start installing
1. Install this application onto your server as the super user
```console
mkdir -p /opt/postcards
curl https://bitbucket.org/skyetel/postcards-installer/get/master.tar.gz | tar xvz --strip 1 -C /opt/postcards
```
2. Start the configuration, and build process
```console
cd /opt/postcards
./run.sh
```
3. Enter your configuration
```console
Enter your public DNS name (ex: talk.example.com): myserver.example.com
Enter an email where you can be reached (ex: me@example.com): me@myserver.example.com
Enter a password to protect your administrator account: DontUseThisPassword1
Enter a name for your app: Postcards
Enter a URL for your app logo (https only): https://mycoolwebsite.com/myawesomelogo.png
Enter your login.skyetel.com API SID: XXXXXXXXXXX
Enter your login.skyetel.com API Secret: XXXXXXXXXX
```

Your brand new application will need a few minutes to build and start for the very first time. During that time it will setup the application with all your settings and will finally report back to you when it is completed with the following message:

```console
In a few moments vist https://myserver.example.com/backend/admin in your browser

Username: admin
Password: DontUseThisPassword1

----------------

Setup your Skyetel SMS & MMS setting on your selected phonenumbers as follows: 

Forward:  Callback URL
URL:      https://myserver.example.com/backend/api/in?key=fa32f9ec93ca356
Method:   POST
```

## Setting up your Users and Phone Numbers
1. Visit the Skyetel dashboard https://login.skyetel.com and update your Skyetel Phone Number with the newly created Callback URL.
2. Visit your new Administrator console https://myserver.example.com/backend/admin and login with "admin" user and your administrator password.
2. Create a new user with an email address, display name, and password.
3. Create a phone number record that matches your Skyetel SMS/MMS enabled Phone Number
4. Assign your phone number to a user

## Use the Postcards user interface to make someone's day!
1. Login to your Postcards user interface by visiting your website https://myserver.example.com with a user email address and password setup by the Postcards Administrator
2. Send an SMS or picture MMS to your favorite contact Phone Number

## Minor Version Updates
#### Warning: We highly recommended backing up your system before updating and expect a few minutes to re-build the app.
Re-Install this application onto your server as the super user in the root directory
```console
curl https://bitbucket.org/skyetel/postcards-installer/get/master.tar.gz | tar xvz --strip 1 -C /opt/postcards --overwrite
```
Start the configuration, and build process
```console
cd /opt/postcards
./run.sh
```
Enter your configuration (hit enter to use previous values)
```console
Would you like to reconfigure (Y/N)? y
```
Restart your application
```console
Would you like to restart it (Y/N)? y
```
## Troubleshooting
* Failure to Build App
    * Increase system memory to at least the minimum recommended 4GB
    * Verify you are using one of the supported Operating Systems
    * (CentOS 8 users): CentOS 8 is not fully support by Docker installation but it should be released shortly: https://github.com/docker/docker-install/issues/154

* Not receiving messages
    * Check the Skyetel Portal SMS/MMS logs for any error messages (see error examples below)  
    ![Error Examples](docs/postcards_callback_error_examples.png)
    * Verify the Callback URL matches your Phone Number Callback URL in the Skyetel Portal and is using the correct domain protocol http/https.
    * Verify Skyetel can send POST requests to the Callback URL.



## Postcards Support
Postcards is offered as an open source, free project and we think you will love it. However, Skyetel Support has not been trained extensively on Postcards and cannot offer support for Postcards related issues. If you have technical issues with Postcards or would like to submit a bug report or feature request, please [visit this link](https://skyetel.atlassian.net/secure/CreateIssueDetails!init.jspa?pid=10202&issuetype=10200&priority=4&summary=Feedback%20on%20Postcards).
