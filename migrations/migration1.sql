use sms_mms_app;

CREATE TABLE IF NOT EXISTS `templates` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned DEFAULT NULL,
  `message` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE IF NOT EXISTS `announcements` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned DEFAULT NULL,
  `message` text NOT NULL,
  `phonenumbers` varchar(750) NOT NULL,
  `failed_numbers` varchar(750) NULL,
  `process_id` varchar(100) NULL,
  `state` varchar(100) NOT NULL,
  `scheduled` datetime(6) NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE IF NOT EXISTS `smtp` (
  `host` varchar(255) NOT NULL,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `port` varchar(255) NOT NULL,
  `sender_email` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

SET @s = (SELECT IF(
    (SELECT COUNT(*)
        FROM INFORMATION_SCHEMA.COLUMNS
        WHERE table_name = 'contacts'
        AND table_schema = DATABASE()
        AND column_name = 'archived'
    ) > 0,
    "SELECT 1",
    "ALTER TABLE contacts ADD `archived` tinyint(1) NOT NULL DEFAULT '0'"
));

PREPARE stmt FROM @s;
EXECUTE stmt;
DEALLOCATE PREPARE stmt;

SET @s = (SELECT IF(
    (SELECT COUNT(*)
        FROM INFORMATION_SCHEMA.COLUMNS
        WHERE table_name = 'contacts'
        AND table_schema = DATABASE()
        AND column_name = 'auto_reply'
    ) > 0,
    "SELECT 1",
    "ALTER TABLE contacts ADD `auto_reply` text"
));

PREPARE stmt FROM @s;
EXECUTE stmt;
DEALLOCATE PREPARE stmt;

SET @s = (SELECT IF(
    (SELECT COUNT(*)
        FROM INFORMATION_SCHEMA.COLUMNS
        WHERE table_name = 'dialogs'
        AND table_schema = DATABASE()
        AND column_name = 'archived'
    ) > 0,
    "SELECT 1",
    "ALTER TABLE dialogs ADD `archived` tinyint(1) NOT NULL DEFAULT '0'"
));

PREPARE stmt FROM @s;
EXECUTE stmt;
DEALLOCATE PREPARE stmt;

SET @s = (SELECT IF(
    (SELECT COUNT(*)
        FROM INFORMATION_SCHEMA.COLUMNS
        WHERE table_name = 'users'
        AND table_schema = DATABASE()
        AND column_name = 'notifications_enabled'
    ) > 0,
    "SELECT 1",
    "ALTER TABLE users ADD `notifications_enabled` tinyint(1) NOT NULL DEFAULT '0'"
));

PREPARE stmt FROM @s;
EXECUTE stmt;
DEALLOCATE PREPARE stmt;