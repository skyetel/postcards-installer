#!/usr/bin/env bash

while [ true ]
do
	php /var/www/html/backend/announcements-worker.php --verbose --no-interaction &
	sleep 1
done