FROM php:5.6-apache
MAINTAINER Skyetel <support@skyetel.com>

# Install dependent packages
RUN curl -sL https://deb.nodesource.com/setup_12.x | bash - && \
    apt-get -qq update > /dev/null && \
    apt-get -qq install  \
        libmcrypt-dev \
        git \
        nodejs npm \
        unzip > /dev/null \
    && rm -r /var/lib/apt/lists/*

# Register the COMPOSER_HOME environment variable
ENV COMPOSER_HOME /composer

# Add global binary directory to PATH and make sure to re-export it
ENV PATH /composer/vendor/bin:$PATH

# Allow Composer to be run as root
ENV COMPOSER_ALLOW_SUPERUSER 1

# Enable Apache Mod Rewrite and install Composer
RUN a2enmod rewrite headers \
    && docker-php-ext-install mcrypt pdo_mysql \
    && curl -o /tmp/composer-setup.php https://getcomposer.org/installer \
    && curl -o /tmp/composer-setup.sig https://composer.github.io/installer.sig \
    && php -r "if (hash('SHA384', file_get_contents('/tmp/composer-setup.php')) !== trim(file_get_contents('/tmp/composer-setup.sig'))) { unlink('/tmp/composer-setup.php'); echo 'Invalid installer' . PHP_EOL; exit(1); }" \
    && php /tmp/composer-setup.php --install-dir=/usr/local/bin --filename=composer

ARG REBUILD_DEPS=false

ARG REACT_APP_TITLE=Postcards
ENV REACT_APP_TITLE $REACT_APP_TITLE

ARG REACT_APP_LOGO_URL=https://postcards.skyetel.com/favicon.png
ENV REACT_APP_LOGO_URL $REACT_APP_LOGO_URL

# Install UI
ARG UI_TAG=master
ARG UI_URL=https://bitbucket.org/skyetel/postcards-ui/get/${UI_TAG}.tar.gz
RUN curl -H 'Cache-Control: no-cache' ${UI_URL} | tar xz --strip 1 -C /tmp \
    && npm install --prefix /tmp /tmp \
    && npm run build --prefix /tmp \
    && mv /tmp/build/* /var/www/html \
    && rm -rf /tmp/*

ARG BACKEND_TAG=master
ARG BACKEND_URL=https://bitbucket.org/skyetel/postcards-backend/get/${BACKEND_TAG}.tar.gz

RUN mkdir -p /var/www/html/backend \
    && curl -H 'Cache-Control: no-cache' ${BACKEND_URL} 2> /dev/null | tar xz --strip 1 --exclude='*.md' -C /var/www/html/backend \
    && composer install -q -n -d /var/www/html/backend \
    && echo -e 'Order allow,deny\nDeny from all' > /var/www/html/backend/vendor/.htaccess \
    && rm -rf /var/www/html/backend/composer.* \
    && mkdir -p /var/www/attachments \
    && chmod ugo+rwx /var/www/attachments
